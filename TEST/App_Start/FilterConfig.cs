﻿using System.Web;
using System.Web.Mvc;

namespace TEST {
    public class FilterConfig {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
            filters.Add(new HandleErrorAttribute());
            //將內建的許可權過濾器新增到全域性過濾中
            //filters.Add(new AuthorizeAttribute());
        }
    }
}
