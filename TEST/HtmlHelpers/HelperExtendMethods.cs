﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace TEST.HtmlHelpers {
    public static class HelperExtendMethods {
        public static MvcHtmlString IsDisable(this MvcHtmlString htmlString, bool isDisable) {
            if (isDisable) {
                var html = htmlString.ToString();
                var disabled = " disabled=\"disabled\" ";
                html = html.Insert(html.IndexOf(">", StringComparison.Ordinal), disabled);
                return new MvcHtmlString(html);
            }
            return htmlString;
        }
        public static MvcHtmlString IsCheck(this MvcHtmlString htmlString, bool isDisable) {
            if (isDisable) {
                var html = htmlString.ToString();
                var disabled = " checked=\"checked\" ";
                html = html.Insert(html.IndexOf(">", StringComparison.Ordinal), disabled);
                return new MvcHtmlString(html);
            }
            return htmlString;
        }
    }
}