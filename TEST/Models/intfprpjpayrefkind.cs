//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace TEST.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class intfprpjpayrefkind
    {
        public string CERTITYPE { get; set; }
        public string CERTINO { get; set; }
        public string POLICYNO { get; set; }
        public decimal SERIALNO { get; set; }
        public string PAYREFREASON { get; set; }
        public decimal ITEMKINDNO { get; set; }
        public string RISKCODE { get; set; }
        public string KINDCODE { get; set; }
        public string ARTICLECODE { get; set; }
        public string CLAUSETYPE { get; set; }
        public Nullable<decimal> KINDFEE { get; set; }
        public decimal KINDFEERATE { get; set; }
        public string COMCODE { get; set; }
        public string LOSSTYPE { get; set; }
        public Nullable<decimal> REALPAYREFFEE { get; set; }
        public string FLAG { get; set; }
        public string SUBRISKCODE { get; set; }
        public string PLANFEECURRENCY { get; set; }
        public decimal PLANFEECNY { get; set; }
        public decimal EXCHANGERATE { get; set; }
        public string ONACCFLAG { get; set; }
        public string REALPAYREFFLAG { get; set; }
        public System.DateTime INPUTDATE { get; set; }
        public string ACCREASON { get; set; }
        public Nullable<decimal> REALPAYREFFEECNY { get; set; }
        public Nullable<System.DateTime> PAYREFDATE { get; set; }
        public Nullable<decimal> SHORTFEE { get; set; }
        public decimal PAYREFTIMES { get; set; }
    }
}
