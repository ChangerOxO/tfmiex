//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace TEST.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class intfprpbadditionaldrivers
    {
        public string POLICYNO { get; set; }
        public string SERIALNO { get; set; }
        public string ADDINSUREDNAME { get; set; }
        public string ADDIDENTIFYNUMBER { get; set; }
        public System.DateTime ADDBIRTHDAY { get; set; }
        public string ADDINSUREDIDENTITY { get; set; }
        public string BENEFITTYPE { get; set; }
        public string SEQUENCENO { get; set; }
        public string PAYPROPORTION { get; set; }
        public string ADDINSUREDIDENTITY2 { get; set; }
        public string ADDINSUREDNAME2 { get; set; }
        public string ADDIDENTIFYNUMBER2 { get; set; }
        public string POSTADDRESS { get; set; }
        public string PHONENUMBER { get; set; }
        public Nullable<System.DateTime> beneficiarydate { get; set; }
    }
}
