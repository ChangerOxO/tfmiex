//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace TEST.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class FirEdrApplMains
    {
        public string PolicyNo { get; set; }
        public decimal ApplSeq { get; set; }
        public string ApplNo { get; set; }
        public string PayStatus { get; set; }
        public string IssuId { get; set; }
        public string IssuName { get; set; }
        public string ObjAddress { get; set; }
        public Nullable<decimal> TotalCover { get; set; }
        public Nullable<System.DateTime> EffDate { get; set; }
        public Nullable<System.DateTime> ExpDate { get; set; }
        public string EdrType1 { get; set; }
        public string EdrType2 { get; set; }
        public string EdrType3 { get; set; }
        public string EdrType4 { get; set; }
        public string EdrType5 { get; set; }
        public string EdrType6 { get; set; }
        public string EdrType7 { get; set; }
        public string EdrType8 { get; set; }
        public Nullable<System.DateTime> EdrEffDate { get; set; }
        public string ApplierFlag { get; set; }
        public string CoverChgFlag { get; set; }
        public Nullable<decimal> CoverChgAmt { get; set; }
        public Nullable<decimal> CoverFinalAmt { get; set; }
        public string StrucChgFlag1 { get; set; }
        public string StrucChgFlag2 { get; set; }
        public string StrucChgFlag3 { get; set; }
        public string StrucChgFlag4 { get; set; }
        public string ConstructWall { get; set; }
        public string ConstructRoof { get; set; }
        public Nullable<decimal> UsedArea { get; set; }
        public Nullable<decimal> FloorCnt { get; set; }
        public string MortChgFlag { get; set; }
        public string MortBankNoGer { get; set; }
        public string MortBankNo { get; set; }
        public string MortBankName { get; set; }
        public string ReissueFlag1 { get; set; }
        public string ReissueFlag2 { get; set; }
        public string ReissueFlag3 { get; set; }
        public string ReissueFlag4 { get; set; }
        public string TransType { get; set; }
        public string TransId { get; set; }
        public string TransName { get; set; }
        public string TransOccu { get; set; }
        public string CancelFlag { get; set; }
        public string CancelInsurer { get; set; }
        public string CancelOthers { get; set; }
        public string ReduceReason { get; set; }
        public string ReductInsurer { get; set; }
        public string ReductOthers { get; set; }
        public string ReductDoc1 { get; set; }
        public string ReductDoc2 { get; set; }
        public string ReductDoc3 { get; set; }
        public string ReductDoc4 { get; set; }
        public string ReductDoc5 { get; set; }
        public string ReductDoc6 { get; set; }
        public string DocClaimFlag1 { get; set; }
        public string DocClaimFlag2 { get; set; }
        public string DocClaimFlag3 { get; set; }
        public string RefundWay { get; set; }
        public string RefundBankNoGer { get; set; }
        public string RefundBankNo { get; set; }
        public string RefundBankName { get; set; }
        public string RefundAccountNo { get; set; }
        public string RefundCheckFlag { get; set; }
        public string RefundPostCode { get; set; }
        public string RefundLocCounty { get; set; }
        public string RefundLocArea { get; set; }
        public string RefundAddress { get; set; }
        public string RefundPolicyNo { get; set; }
        public string EndorseRemark { get; set; }
        public string ChannelId { get; set; }
        public string ChannelName { get; set; }
        public string ChannelType { get; set; }
        public string ChnlUnitId { get; set; }
        public string ChnlUnitName { get; set; }
        public string ChnlAgentId { get; set; }
        public string ChnlAgentName { get; set; }
        public string ChnlAgentBussNo { get; set; }
        public string PolAgentId { get; set; }
        public string PolAgentName { get; set; }
        public string PolCompId { get; set; }
        public string PolUnitId { get; set; }
        public string PolUnitName { get; set; }
        public string PolOfficerId { get; set; }
        public string PolOfficerUnitId { get; set; }
        public string PolUnitId2 { get; set; }
        public string BusinessSource { get; set; }
        public string BusinessSrcName { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string CreateUser { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public string UpdateUser { get; set; }
        public string RefundAccountName { get; set; }
    }
}
