//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace TEST.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TaApplApplicants
    {
        public string TransactionNo { get; set; }
        public decimal TransNoSeq { get; set; }
        public decimal ApplSeq { get; set; }
        public string ApplId { get; set; }
        public string ApplIdType { get; set; }
        public string ApplSexFlag { get; set; }
        public string ApplName { get; set; }
        public string ApplType { get; set; }
        public string ApplTel { get; set; }
        public string ApplMobile { get; set; }
        public string ApplEMail { get; set; }
        public string ApplPostCode { get; set; }
        public string ApplLocCounty { get; set; }
        public string ApplLocArea { get; set; }
        public string ApplAddress { get; set; }
        public string ApplNationality { get; set; }
        public string ApplHighRiskOccu { get; set; }
        public string ApplOccupation { get; set; }
        public string ApplResidence { get; set; }
        public Nullable<System.DateTime> ApplBirthday { get; set; }
        public string ApplOwnerId { get; set; }
        public string ApplOwnerName { get; set; }
        public string ApplOwnerStock { get; set; }
        public string ApplOwnerBearer { get; set; }
        public string InsuredRel { get; set; }
        public string InsuredRelName { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string CreateUser { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public string UpdateUser { get; set; }
    }
}
