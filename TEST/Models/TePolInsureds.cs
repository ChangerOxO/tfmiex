//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace TEST.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TePolInsureds
    {
        public string TransactionNo { get; set; }
        public decimal TransNoSeq { get; set; }
        public decimal InsuSeq { get; set; }
        public string InsuId { get; set; }
        public string InsuIdType { get; set; }
        public string InsuName { get; set; }
        public string InsuType { get; set; }
        public string InsuTel { get; set; }
        public string InsuMobile { get; set; }
        public string InsuEMail { get; set; }
        public string InsuPostCode { get; set; }
        public string InsuLocCounty { get; set; }
        public string InsuLocArea { get; set; }
        public string InsuAddress { get; set; }
        public string InsuBusinessAddr { get; set; }
        public string InsuNationality { get; set; }
        public string InsuHighRiskOccu { get; set; }
        public string InsuOccupation { get; set; }
        public string InsuResidence { get; set; }
        public Nullable<System.DateTime> InsuBirthday { get; set; }
        public string InsuOwnerId { get; set; }
        public string InsuOwnerName { get; set; }
        public string InsuOwnerStock { get; set; }
        public string InsuOwnerBearer { get; set; }
        public string InsuredRelName { get; set; }
        public string AgencyType { get; set; }
        public Nullable<decimal> SubCompanyCnt { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string CreateUser { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public string UpdateUser { get; set; }
    }
}
