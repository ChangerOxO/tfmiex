//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace TEST.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class AutoEdrApplInstypes
    {
        public string PolicyNo { get; set; }
        public decimal ApplSeq { get; set; }
        public decimal InsSeq { get; set; }
        public string EdrType { get; set; }
        public string InsId { get; set; }
        public Nullable<decimal> Cover { get; set; }
        public string InputType { get; set; }
        public string MainInsTypeFlag { get; set; }
        public Nullable<decimal> TotalCover { get; set; }
        public string DeductType { get; set; }
        public string DeductValue { get; set; }
        public Nullable<decimal> CoverPerPerson { get; set; }
        public Nullable<decimal> CoverPerIncident { get; set; }
        public Nullable<decimal> CoverTimes { get; set; }
        public Nullable<decimal> DaysCnt { get; set; }
        public string TransType { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string CreateUser { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public string UpdateUser { get; set; }
    }
}
