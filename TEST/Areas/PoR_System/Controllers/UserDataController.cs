﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TEST.Models;
using TEST.Areas.PoR_System.Models;
using Newtonsoft.Json;


namespace TEST.Areas.PoR_System.Controllers {
    public class UserDataController : Controller {
        private tlgDbEntities db = new tlgDbEntities();

        // GET: PoR_System/UserData
        public ActionResult Index() {
            var model = new UserDataSearchViewModel();
            var channels = db.InsChannels.ToList();
            var units = db.SysUnits.ToList();

            var channelsData = new List<Dictionary<string, string>>();
            var unitsData = new List<Dictionary<string, string>>();

            foreach (var item in channels) {
                var map = new Dictionary<string, string>();
                map.Add("code", item.ChannelId);
                map.Add("value", item.ChannelName);
                channelsData.Add(map);
            }
            foreach (var item in units) {
                var map = new Dictionary<string, string>();
                map.Add("code", item.UnitId);
                map.Add("parentCode", item.ChannelId);
                map.Add("value", item.UnitName);
                unitsData.Add(map);
            }

            model.Channels = JsonConvert.SerializeObject(channelsData);
            model.Units = JsonConvert.SerializeObject(unitsData);

            return View(model);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Index(UserDataSearchViewModel model) {
            var query = from user in db.SysUserDatas
                        join unit in db.SysUnits on user.UnitId equals unit.UnitId
                        join channel in db.InsChannels on user.ChannelId equals channel.ChannelId
                        where (model.Condition.ChannelId == null || user.ChannelId == model.Condition.ChannelId)
                        && (model.Condition.UnitId == null || user.UnitId == model.Condition.UnitId)
                        && (model.Condition.UserId == null || user.UserId == model.Condition.UserId)
                        && (model.Condition.UserName == null || user.UserName == model.Condition.UserName)
                        select new UserDataSearchTableViewModel { FullName = channel.ChannelName + "-" + unit.UnitName, UserData = user };

            model.DataTable = query.ToList();
            model.DataCount = query.Count();

            return View(model);
        }

        public ActionResult Edit(string channelId, string unitId, string userId) {
            var model = new UserDataInsertViewModel();

            var sysRoles = new List<SysCodes>();
            var sysRolesOpt1 = new List<SysCodes>();
            var sysRolesOpt2 = new List<SysCodes>();

            sysRolesOpt1 = db.SysCodes.Where(m => m.CodeType == "SysRole").ToList();
            sysRolesOpt2 = db.SysCodes.Where(m => m.CodeType == "ChannelSysRole").ToList();

            sysRoles = sysRolesOpt1;
            //if ("1".equals(b2bUser.getChannelType()))
            //    sysRoles = sysRolesOpt1;
            //else
            //    sysRoles = sysRolesOpt2;

            SysUserDatas user = null;
            var emptyUser = new SysUserDatas();
            var userroleids = new List<string>();
            var insAuthLevelDtls = new InsAuthLevelDtls();

            if (userId != null) {
                user = db.SysUserDatas.Find(channelId, userId);
                userroleids = db.SysUserRoles.Where(x => x.ChannelId == channelId && x.UserId == userId).Select(x => x.UserRoleId).ToList();
                var dtls = db.InsAuthLevelDtls.Where(x => x.UserId == userId && x.InsClass == "F").ToList();
                if (dtls.Count > 0)
                    insAuthLevelDtls = dtls[0];
            }
            else {
                if (channelId != null && unitId != null) {
                    emptyUser.ChannelId = channelId;
                    emptyUser.UnitId = unitId;
                }
            }

            model.Channels = db.InsChannels.ToList();
            model.Units = db.SysUnits.ToList();
            model.SysRoles = sysRoles;
            model.SysRolesOpt1 = sysRolesOpt1;
            model.SysRolesOpt2 = sysRolesOpt2;
            model.UserData = user ?? emptyUser;
            model.InsAuthLevelDtls = insAuthLevelDtls;
            model.UserRoleIds = JsonConvert.SerializeObject(userroleids);
            model.Operation = string.IsNullOrEmpty(userId) ? "insert" : "update";

            return View(model);
        }

        [HttpPost]
        public ActionResult Insert(UserDataInsertParamModel model) {
            var record = new SysUserDatas();
            record.ChannelId = model.ChannelId;
            record.UnitId = model.UnitId;
            record.UserName = model.UserName;
            record.UserId = model.UserId;
            record.AgentFlag = model.AgentFlag;
            record.UserMail = model.UserMail;
            record.PersonalMail = model.PersonalMail;
            record.AgentId = model.AgentId;
            record.AgentRegNo = model.AgentRegNo;
            record.AgentTel = model.AgentTel;
            record.AgentMobile = model.AgentMobile;
            record.AgentAddress = model.AgentAddress;
            record.BusinessSrc = model.BusinessSrc;
            record.CommSetupNo = model.CommSetupNo;
            record.CommTypeFlag = model.CommTypeFlag;
            record.SupervisorFlag = model.SupervisorFlag;
            record.SupervisorUnitId = model.SupervisorUnitId;
            record.UserSuspendFlag = model.UserSuspendFlag;
            record.InsAgentId = model.InsAgentId;
            record.MobileFlag = model.MobileFlag;
            if (model.UserSuspendFlag == "Y") record.UserSuspendDate = new DateTime();
            record.AgentSuspendFlag = model.AgentSuspendFlag;
            if (model.AgentSuspendFlag == "Y") record.AgentSuspendDate = new DateTime();
            record.EffDate = model.EffDate;
            record.ExpDate = model.ExpDate;
            record.SuspendReason = model.SuspendReason;
            record.AutoDirectBusiness = model.AutoDirectBusiness;

            int result = 0;
            var customers = db.Set<SysUserDatas>();
            customers.Add(record);

            try {
                //result = userdatasMapper.insertSelective(record);
                result = db.SaveChanges();
            }
            catch (Exception) {
                //throw new B2bDuplicateKeyException("使用者資料", "使用者代號", model.UserId(), "userId");
                throw;
            }

            DeleteUserRoles(record);
            if (model.UserRoleIds != null) {
                InsertUserRoles(record, model.UserRoleIds);
            }
            DeleteInsAuthLevelDtls(model.UserId);
            if (model.AuthLevel != null) {
                InsertInsAuthLevelDtls(model);
            }

            return Json(result);
        }
        [HttpPut]
        public ActionResult Update(UserDataInsertParamModel model) {
            var data = db.SysUserDatas.Find(model.ChannelId, model.UserId);
            data.UserName = model.UserName;
            data.UserId = model.UserId;
            data.AgentFlag = model.AgentFlag;
            data.UserMail = model.UserMail;
            data.PersonalMail = model.PersonalMail;
            data.AgentId = model.AgentId;
            data.AgentRegNo = model.AgentRegNo;
            data.AgentTel = model.AgentTel;
            data.AgentMobile = model.AgentMobile;
            data.AgentAddress = model.AgentAddress;
            data.BusinessSrc = model.BusinessSrc;
            data.CommSetupNo = model.CommSetupNo;
            data.CommTypeFlag = model.CommTypeFlag;
            data.SupervisorFlag = model.SupervisorFlag;
            data.SupervisorUnitId = model.SupervisorUnitId;
            data.EffDate = model.EffDate;
            data.ExpDate = model.ExpDate;
            data.UserSuspendFlag = model.UserSuspendFlag;
            data.AgentSuspendFlag = model.AgentSuspendFlag;
            data.InsAgentId = model.InsAgentId;
            data.MobileFlag = model.MobileFlag;

            // 找出原本user資料 與輸入的參數停權註記做判斷
            if (data.UserSuspendFlag == "Y" && model.UserSuspendFlag == "N")
                data.UserReinstateDate = new DateTime();

            // 批次匯入沒設定為null 視為預設值N
            if ((data.UserSuspendFlag == "N" || string.IsNullOrEmpty(data.UserSuspendFlag)) && model.UserSuspendFlag == "Y")
                data.UserSuspendDate = new DateTime();
            if (data.AgentSuspendFlag == "Y" && model.AgentSuspendFlag == "N")
                data.AgentReinstateDate = new DateTime();
            if ((data.AgentSuspendFlag == "N" || string.IsNullOrEmpty(data.AgentSuspendFlag)) && model.AgentSuspendFlag == "Y")
                data.AgentSuspendDate = new DateTime();

            data.SuspendReason = model.SuspendReason;
            data.AutoDirectBusiness = model.AutoDirectBusiness;

            int result = db.SaveChanges();
            if (result == 0) {
                //throw new B2bNoMatchException("使用者資料", "使用者代號", model.UserId(), "userId");
                //throw;
            }
            DeleteUserRoles(data);
            if (model.UserRoleIds != null) {
                InsertUserRoles(data, model.UserRoleIds);
            }
            DeleteInsAuthLevelDtls(model.UserId);
            if (model.AuthLevel != null) {
                InsertInsAuthLevelDtls(model);
            }

            return Json(result);
        }
        [HttpDelete]
        public ActionResult Delete(UserDataDeleteParamModel model) {
            var key = db.SysUserDatas.Find(model.ChannelId, model.UserId);
            db.SysUserDatas.Remove(key);
            int result = db.SaveChanges();
            if (result == 0) {
                //throw new B2bNoMatchException("使用者資料", "使用者代號", param.getUserId(), "userId");
            }
            DeleteUserRoles(key);
            DeleteInsAuthLevelDtls(model.UserId);

            return Json(result);
        }

        private int InsertUserRoles(SysUserDatas key, List<string> userRoleIds) {
            int result = 0;
            foreach (var userRoleId in userRoleIds) {
                var record = new SysUserRoles();
                record.ChannelId = key.ChannelId;
                record.UserId = key.UserId;
                record.UserRoleId = userRoleId;
                var customers = db.Set<SysUserRoles>();
                customers.Add(record);
                result += db.SaveChanges();
            }
            return result;
        }

        private int DeleteUserRoles(SysUserDatas key) {
            var sysUserRole = db.SysUserRoles.Where(x => x.ChannelId == key.ChannelId && x.UserId == key.UserId).FirstOrDefault();
            if (sysUserRole == null) {
                return 0;
            }
            else {
                db.SysUserRoles.Remove(sysUserRole);
                return db.SaveChanges();
            }
        }

        private int InsertInsAuthLevelDtls(UserDataInsertParamModel param) {
            int result = 0;

            var record = new InsAuthLevelDtls();
            record.AuthLevel = param.AuthLevel;
            record.InsClass = "F";
            record.UserId = param.UserId;
            record.CreateDate = new DateTime();

            var customers = db.Set<InsAuthLevelDtls>();
            customers.Add(record);
            result += db.SaveChanges();

            return result;
        }

        private int DeleteInsAuthLevelDtls(string userId) {
            var insAuthLevelDtl = db.InsAuthLevelDtls.Where(x => x.InsClass == "F" && x.UserId == userId).FirstOrDefault();
            if (insAuthLevelDtl == null) {
                return 0;
            }
            else {
                db.InsAuthLevelDtls.Remove(insAuthLevelDtl);
                return db.SaveChanges();
            }
        }
    }
}