﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TEST.Models;

namespace TEST.Areas.PoR_System.Controllers
{
    public class SysUserDatasController : Controller
    {
        private tlgDbEntities db = new tlgDbEntities();

        // GET: PoR_System/SysUserDatas
        public async Task<ActionResult> Index()
        {
            return View(await db.SysUserDatas.ToListAsync());
        }

        // GET: PoR_System/SysUserDatas/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SysUserDatas sysUserDatas = await db.SysUserDatas.FindAsync(id);
            if (sysUserDatas == null)
            {
                return HttpNotFound();
            }
            return View(sysUserDatas);
        }

        // GET: PoR_System/SysUserDatas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PoR_System/SysUserDatas/Create
        // 若要免於大量指派 (overposting) 攻擊，請啟用您要繫結的特定屬性，
        // 如需詳細資料，請參閱 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ChannelId,UnitId,UserId,UserName,UserPasswd,AgentFlag,UserMail,PersonalMail,UserSuspendFlag,UserSuspendDate,UserReinstateDate,AgentTel,AgentMobile,AgentAddress,AgentRegNo,AgentSuspendFlag,AgentSuspendDate,AgentReinstateDate,BusinessSrc,CommSetupNo,InsAgentId,CommTypeFlag,MobileFlag,EffDate,ExpDate,CreateDate,CreateUser,UpdateDate,UpdateUser,AgentId,SuspendReason,SupervisorFlag,SupervisorUnitId,LastVisitDl,LastVisitTodo,LastVisitNews,AutoDirectBusiness")] SysUserDatas sysUserDatas)
        {
            if (ModelState.IsValid)
            {
                db.SysUserDatas.Add(sysUserDatas);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(sysUserDatas);
        }

        // GET: PoR_System/SysUserDatas/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SysUserDatas sysUserDatas = await db.SysUserDatas.FindAsync(id);
            if (sysUserDatas == null)
            {
                return HttpNotFound();
            }
            return View(sysUserDatas);
        }

        // POST: PoR_System/SysUserDatas/Edit/5
        // 若要免於大量指派 (overposting) 攻擊，請啟用您要繫結的特定屬性，
        // 如需詳細資料，請參閱 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ChannelId,UnitId,UserId,UserName,UserPasswd,AgentFlag,UserMail,PersonalMail,UserSuspendFlag,UserSuspendDate,UserReinstateDate,AgentTel,AgentMobile,AgentAddress,AgentRegNo,AgentSuspendFlag,AgentSuspendDate,AgentReinstateDate,BusinessSrc,CommSetupNo,InsAgentId,CommTypeFlag,MobileFlag,EffDate,ExpDate,CreateDate,CreateUser,UpdateDate,UpdateUser,AgentId,SuspendReason,SupervisorFlag,SupervisorUnitId,LastVisitDl,LastVisitTodo,LastVisitNews,AutoDirectBusiness")] SysUserDatas sysUserDatas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sysUserDatas).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(sysUserDatas);
        }

        // GET: PoR_System/SysUserDatas/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SysUserDatas sysUserDatas = await db.SysUserDatas.FindAsync(id);
            if (sysUserDatas == null)
            {
                return HttpNotFound();
            }
            return View(sysUserDatas);
        }

        // POST: PoR_System/SysUserDatas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            SysUserDatas sysUserDatas = await db.SysUserDatas.FindAsync(id);
            db.SysUserDatas.Remove(sysUserDatas);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
