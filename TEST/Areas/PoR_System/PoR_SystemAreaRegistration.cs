﻿using System.Web.Mvc;

namespace TEST.Areas.PoR_System {
    public class PoR_SystemAreaRegistration : AreaRegistration {
        public override string AreaName {
            get {
                return "PoR_System";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) {
            context.MapRoute(
                "PoR_System_default",
                "Sys/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}