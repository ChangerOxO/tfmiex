﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TEST.Models;

namespace TEST.Areas.PoR_System.Models {
    public class UserDataSearchViewModel {
        public UserDataSearchConViewModel Condition { get; set; }
        public List<UserDataSearchTableViewModel> DataTable { get; set; }
        public int DataCount { get; set; }
        public string Channels { get; set; }
        public string Units { get; set; }
    }
    public class UserDataSearchConViewModel {
        [Display(Name = "單位")]
        public string ChannelId { get; set; }

        [Display(Name = "子單位")]
        public string UnitId { get; set; }

        [Display(Name = "使用者代號")]
        public string UserId { get; set; }

        [Display(Name = "使用者姓名")]
        public string UserName { get; set; }

        [Display(Name = "是否有效")]
        public bool InEff { get; set; }
    }
    public class UserDataSearchTableViewModel {
        [Display(Name = "單位")]
        public string FullName { get; set; }

        public SysUserDatas UserData { get; set; }
    }

    public class UserDataInsertViewModel {
        public SysUserDatas UserData { get; set; }
        public List<InsChannels> Channels { get; set; }
        public List<SysUnits> Units { get; set; }
        public List<SysCodes> SysRoles { get; set; }
        public List<SysCodes> SysRolesOpt1 { get; set; }
        public List<SysCodes> SysRolesOpt2 { get; set; }
        public InsAuthLevelDtls InsAuthLevelDtls { get; set; }
        public string UserRoleIds { get; set; }
        public string Operation { get; set; }
    }

    public class UserDataInsertParamModel : UserDataDeleteParamModel {
        public string UnitId { get; set; }
        public string UserName { get; set; }
        public string InsAgentId { get; set; }
        public string AgentFlag { get; set; }
        public string UserMail { get; set; }
        public string PersonalMail { get; set; }
        public string AgentId { get; set; }
        public string AgentRegNo { get; set; }
        public string AgentTel { get; set; }
        public string AgentMobile { get; set; }
        public string AgentAddress { get; set; }
        public string BusinessSrc { get; set; }
        public string CommSetupNo { get; set; }
        public string CommTypeFlag { get; set; }
        public string SupervisorFlag { get; set; }
        public string SupervisorUnitId { get; set; }
        public DateTime EffDate { get; set; }
        public DateTime ExpDate { get; set; }
        public string AuthLevel { get; set; }
        public string UserSuspendFlag { get; set; }
        public string AgentSuspendFlag { get; set; }
        public string MobileFlag { get; set; }
        public List<string> UserRoleIds { get; set; }
        public string SuspendReason { get; set; }
        public string AutoDirectBusiness { get; set; }
    }

    public class UserDataDeleteViewModel {

    }

    public class UserDataDeleteParamModel {
        public string ChannelId { get; set; }
        public string UserId { get; set; }
    }
}