//input: 輸入框元件,若有符合的 value ,會將 code 值寫到輸入元件的 code 屬性上

//data: 資料物件,屬性 code:代碼, value:說明, parentCode:父層代碼

//isShow: 是否顯示全部(選填)

//parentId: 父層物件id(選填)

function autocomplete(input, data, isShow, parentId, itemMapper) {
    isShow = !!isShow;
    var currentFocus;
    var parentCode = '';

    if (!itemMapper) {
        itemMapper = 0;
    }
    if (!(itemMapper instanceof Function)) {
        var funcs = [function(item) {
            return item.value;
        }, function(item) {
            return item.code + ' - ' + item.value;
        }];
        itemMapper = funcs[itemMapper];
    }
    if (parentId) {
        document.getElementById(parentId).addEventListener("change", function(e) {
            parentCode = e.target.getAttribute('code');
            input.value = '';
        });
    }
    input.addEventListener("focus", function(e) {
        var event = document.createEvent('Event');
        event.initEvent('input', true, true);
        input.dispatchEvent(event);
    });

    input.getFilteredItems = function() {
        var val = this.value;
        if (!val && !isShow) {
            return;
        }
        return data.filter(function(item) {
            if (parentId && !parentCode && item.parentCode) {
                return false;
            }
            if (parentId && parentCode && !item.parentCode) {
                return false;
            }
            if (parentCode && item.parentCode && parentCode != item.parentCode) {
                return false;
            }
            return true;
        });
    };
    input.addEventListener("input", function(e) {
        var val = this.value;
        closeAllLists();
        if (!val && !isShow) {
            return;
        }
        currentFocus = -1;
        var div = document.createElement("div");
        div.setAttribute("id", this.id + "autocomplete-list");
        div.setAttribute("class", "autocomplete-items");
        div.style.border = "0px"
        div.style.paddingLeft = window.getComputedStyle(this.parentNode, null).getPropertyValue('padding-left');
        this.parentNode.appendChild(div);

        var filteredItems = input.getFilteredItems();
        filteredItems.forEach(function(item) {
            var matchIdx = itemMapper(item).toUpperCase().indexOf(val.toUpperCase());
            if (matchIdx > -1) {
                var optDiv = document.createElement("div");
                optDiv.innerHTML = itemMapper(item).substring(0, matchIdx);
                optDiv.innerHTML += "<strong>" + itemMapper(item).substr(matchIdx, val.length) + "</strong>";
                optDiv.innerHTML += itemMapper(item).substr(matchIdx + val.length);
                optDiv.innerHTML += "<input type='hidden' code='" + item.code + "' value='" + item.value + "'>";
                optDiv.addEventListener("click", function(e) {
                    var optInput = this.getElementsByTagName("input")[0];
                    input.value = optInput.value;
                    closeAllLists(null, 'autocompleteBlur');
                });
                div.appendChild(optDiv);
            }
        });
    });
    input.addEventListener("keydown", function(e) {
        var div = document.getElementById(this.id + "autocomplete-list");
        if (div)
            div = div.getElementsByTagName("div");
        if (e.keyCode == 40) { // down
            currentFocus++;
            addActive(div);
        } else if (e.keyCode == 38) { // up
            currentFocus--;
            addActive(div);
        } else if (e.keyCode == 13) { // ENTER
            e.preventDefault();
            if (currentFocus > -1) {
                if (div)
                    div[currentFocus].click();
            }
        }
    });

    function addActive(div) {
        if (!div)
            return false;
        removeActive(div);
        if (currentFocus >= div.length)
            currentFocus = 0;
        if (currentFocus < 0)
            currentFocus = (div.length - 1);
        div[currentFocus].classList.add("autocomplete-active");
    }

    function removeActive(div) {
        for (var i = 0; i < div.length; i++) {
            div[i].classList.remove("autocomplete-active");
        }
    }

    function closeAllLists(element, autocompleteBlur) {
        var div = document.getElementById(input.id + "autocomplete-list");
        if (div && element != div && element != input) {
            div.parentNode.removeChild(div);
            var matches = data.filter(function(item) {
                return input.value == item.value;
            });
            var code = matches.length > 0 ? matches[0].code : "";
            input.setAttribute("code", code);
            var event = document.createEvent('Event');
            event.initEvent('change', true, true);
            input.dispatchEvent(event);
            if (autocompleteBlur) {
                event = document.createEvent('Event');
                event.initEvent('autocompleteBlur', true, true);
                input.dispatchEvent(event);
            }
        }
    }
    document.addEventListener("click", function(e) {
        closeAllLists(e.target, 'autocompleteBlur');
    });

    input.setParentCode = function(code) {
        parentCode = code || code === 0 ? code : 'xxxxxxxxxx';
        input.value = '';
    };
    input.getCode = function(value) {
        var item_ = data.filter(function(item) {
            return item.value == value;
        });
        if (item_.length == 0) {
            return '';
        }
        return item_[0].code;
    };
    input.getValue = function(code) {
        var item_ = data.filter(function(item) {
            return item.code == code;
        });
        if (item_.length == 0) {
            return '';
        }
        return item_[0].value;
    };
}

function autocompleteGetData(child) {
    var this_ = window;

    var autocompleteData = child.data('autocomplete');
    if (!autocompleteData) {
        var field = child.closest('.autocomplete').find('.data-autocomplete')
        var json = field.text();
        field.detach();
        if (json) {
            autocompleteData = JSON.parse(json);
        }
    }
    return autocompleteData;
}
