//檢查身分證
function isIdentityCardNumber(id){
	id = id.toUpperCase();
	if (!/^[A-Z][1-2]\d{8}$/.test(id))
		return false;
	var tab = 'ABCDEFGHJKLMNPQRSTUVXYWZIO';
	var A1 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3];
	var A2 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5];
	var Mx = [1, 9, 8, 7, 6, 5, 4, 3, 2, 1, 1];
	var index = tab.indexOf(id[0]);
	var sum = 0;
	var number = '' + A1[index] + A2[index] + id.substr(1,9);
	for (var i=0; i<=10; i++) {
		sum += parseInt(number[i]) * Mx[i];
	}
	return sum % 10 == 0 ? true : false;
}

//檢查統編
function isUniformNumber(taxId) {
	if (!/^\d{8}$/.test(taxId) || ['00000000', '11111111'].indexOf(taxId) != -1)
		return false;
	var validateOperator = [ 1, 2, 1, 2, 1, 2, 4, 1 ],
		sum = 0, 
		calculate = function(product) {
			// 個位數 + 十位數
			var ones = product % 10, tens = (product - ones) / 10;
			return ones + tens;
		};
	for (var i = 0; i < validateOperator.length; i++) {
		sum += calculate(taxId[i] * validateOperator[i]);
	}
	return sum % 10 == 0 || (taxId[6] == '7' && (sum + 1) % 10 == 0);
}

//檢查居留證
function isResidentCertNo(id) {
	var tab = 'ABCDEFGHJKLMNPQRSTUVXYWZIO';
	var A1 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3];
	var A2 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5];
	var number;
	id = id.toUpperCase();
	if (/^[A-Z][A-D]\d{8}$/.test(id)) {
		//舊式
		var first = tab.indexOf(id[0]);
		var second = tab.indexOf(id[1]);
		number = '' + A1[first] + A2[first] + A2[second] + id.substr(2,8);
	} else if (/^[A-Z][8-9]\d{8}$/.test(id)) {
		//新式
		var index = tab.indexOf(id[0]);
		number = '' + A1[index] + A2[index] + id.substr(1,9);
	} else
		return false;
	var Mx = [1, 9, 8, 7, 6, 5, 4, 3, 2, 1, 1];
	var sum = 0;
	for (var i=0; i<=10; i++) {
		sum += (parseInt(number[i]) * Mx[i]) % 10;
	}
	return sum % 10 == 0 ? true : false;
}