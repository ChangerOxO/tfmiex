function b2bAjaxCall(settingParam) {
    // prevent continuous clicks
    var caller = b2bAjaxCall.caller;
    var this_ = this;
    var funcName = '';
    var isOnclick = false;
    while (caller) {
        if (caller.name == 'onclick') {
            isOnclick = true;
        }
        funcName = funcName + caller.name;
        try {
            caller = caller.caller;
        } catch (err) {
            caller = null;
        }
    }
    if (isOnclick) {
        if (!this_.pvtClicks) {
            this_.pvtClicks = {};
        }
        if (this_.pvtClicks[funcName]) {
            return;
        }
        this_.pvtClicks[funcName] = true;
        if (this_.$('.loading-mask'))
            this_.$('.loading-mask').show();
    }

    var setting = {
        type: 'POST',
        dataType: 'json'
    }
    Object.assign(setting, settingParam);

    setting.error = function(jqXHR, textStatus, errorThrown) {
        if (!b2bAjaxCall.getErrorFuncs) {
            b2bAjaxCall.getErrorFuncs = {
                '0': function(statusCode) {
                    return {
                        status: 'N',
                        msg: '發送失敗，請聯絡IT人員！',
                        code: '0',
                        response: 'fail'
                    };
                },
                '404': function(statusCode) {
                    return {
                        status: 'N',
                        msg: '指定頁面不存在，請聯絡IT人員',
                        code: '404',
                        response: 'fail'
                    };
                },
                'other': function(statusCode) {
                    return {
                        status: 'N',
                        msg: '伺服器錯誤，錯誤碼 - ' + statusCode,
                        code: '102',
                        response: 'fail'
                    };
                }
            };
        }
        var error;
        try {
            error = JSON.parse(jqXHR.responseText);
        } catch (err) {
            var statusCode = jqXHR.status || '0';
            error = b2bAjaxCall.getErrorFuncs[statusCode](statusCode);
        }
        if (error.code == '998') {
            if (confirm(error.msg)) {
                window.location.href = appUrl.sys + '/login';
                return;
            }
        } else {
            if (settingParam.error) {
                settingParam.error(error, jqXHR, textStatus, errorThrown);
            }
        }
        console.log(error);
        accordionUtil.log(error.msg, 'text-danger');

        if (isOnclick) {
            this_.pvtClicks[funcName] = false;
            if (this_.$('.loading-mask'))
                this_.$('.loading-mask').hide();
        }
    }
    setting.success = function(result) {
        if (settingParam.success) {
            settingParam.success(result);
        }
        if (isOnclick) {
            this_.pvtClicks[funcName] = false;
            if (this_.$('.loading-mask'))
                this_.$('.loading-mask').hide();
        }
    }
    if (setting.type.toUpperCase() != "GET" && !setting.enctype) {
        if (typeof setting.data == "object") {
            setting.data = JSON.stringify(setting.data);
        }
        if (!setting.contentType) {
            setting.contentType = 'application/json; charset=utf-8';
        }
    }
    setting.xhrFields = {
        withCredentials: true
    }
    return $.ajax(setting);
}

function b2bParseUrl(url) {
    var obj = {};
    var keyvalue = [];
    var key = "",
        value = "";
    var paraString = url.substring(url.indexOf("?") + 1, url.length).split("&");
    for (var i in paraString) {
        keyvalue = paraString[i].split("=");
        key = keyvalue[0];
        value = decodeURI(keyvalue[1]);
        obj[key] = value;
    }
    return obj;
}

function b2bFileDownload(settingParam) {
    var this_ = this;
    var setting = {
        type: 'GET',
        contentType: 'application/octet-stream'
    }
    Object.assign(setting, settingParam);
    var request = new XMLHttpRequest();
    request.open(setting.type, setting.url);
    request.responseType = 'blob';
    request.onload = function(oEvent) {
        if (this_.$('.loading-mask'))
            this_.$('.loading-mask').hide();
        var blob = new Blob([this.response], {
            type: this.response.type
        });
        if (request.status == 200) {
            var filename = '';
            var content = request.getResponseHeader("Content-Disposition");
            if (content && content.indexOf('attachment') !== -1) {
                var matches = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/
                    .exec(content);
                if (matches != null && matches[1])
                    filename = matches[1].replace(/['"]/g, '');
            }

            if (setting.success)
                setting.success();

            if (navigator.msSaveOrOpenBlob) {
                // IE
                navigator.msSaveOrOpenBlob(blob, filename);
            } else {
                var a = document.createElement("a");
                a.style = "display: none";
                document.body.appendChild(a);
                var url = window.URL.createObjectURL(blob);
                a.href = url;
                a.download = filename;
                a.click();
                window.URL.revokeObjectURL(url);
            }
        } else {
            if (setting.error) {
                var reader = new FileReader();
                if ("application/json" == this.response.type) {
                    reader.readAsText(blob);
                    reader.onload = function(e) {
                        setting.error(JSON.parse(reader.result));
                    };
                } else
                    setting.error({
                        msg: '檔案下載失敗'
                    });
            }
        }
    };
    if (this_.$('.loading-mask'))
        this_.$('.loading-mask').show();
    if (setting.data)
        request.send(setting.data);
    else
        request.send();
}

function b2bPdfView(pdfUrl, queryStr) {
    var url = basePath + '/webjars/b2b-common/pdf/web/viewer.html?file=' +
        encodeURIComponent(pdfUrl + '?' + queryStr);
    window.open(url);
}

//列印PDF功能禁用下載,ctrl+c,與右鍵
function b2bPdfViewNoDownload(pdfUrl, queryStr) {
    var url = basePath + '/webjars/b2b-common/pdf/web/viewer2.html?file=' +
        encodeURIComponent(pdfUrl + '?' + queryStr);
    window.open(url);
}

function addCommas(str) {
    if (!/^[0-9,.]{1,}$/.test(str))
        return "0";
    //str為string時轉為float
    str = str + '';
    if (str.indexOf(',') > -1) {
        str = parseFloat(str.replace(/,/g, ''));
    }
    str += '';
    splittedStr = str.split('.');
    integer = splittedStr[0];
    decimal = splittedStr.length > 1 ? '.' + splittedStr[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(integer)) {
        integer = integer.replace(rgx, '$1' + ',' + '$2');
    }
    return integer + decimal;
}

function rmCommas(str) {
    if (!/^[0-9,.]{1,}$/.test(str))
        return 0;
    str = str + '';
    if (str.indexOf(',') > -1) {
        str = str.replace(/,/g, '');
    }
    return parseFloat(str);
}

function valueAddCommas(element) {
    element.value = addCommas(element.value);
}

function escapeHTML(str) {
    if (!str)
        return '';
    return str.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
}

function emptySelect(element) {
    element.empty();
    element.append('<option value="">請選擇</option>');
}
