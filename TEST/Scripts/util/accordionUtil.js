//accordionUtil.js
function accordionUtil() {

}
(function(global) {
	var newmsgblock = $('#newmsg');
	var msgsblock = $('#msgs');
	var msg0 = newmsgblock.find('p:eq(0)');
	var msgs = msgsblock.find('div.card-body');
	var i = 0;
	// log
	global.accordionUtil.log = function(msg, clss) {
		// text-muted
		// text-primary
		// text-success
		// text-info
		// text-warning
		// text-danger
		// text-secondary
		// text-dark
		if (!clss)
			clss = 'text-muted';
		var newmsg = msg0.clone();
		newmsg.addClass(clss);
		newmsg.text(msg);
		if (i == 0) {
			msg0.detach();
			newmsg.attr('href', '');
		}
		if (i > 0) {
			var oldmsg = newmsgblock.find('p:eq(0)');
			oldmsg.removeClass('dropdown-toggle');
			oldmsg.appendTo(msgs);
			newmsg.addClass('dropdown-toggle');
		}
		newmsg.appendTo(newmsgblock);
		i += 1;
	}
}(this))