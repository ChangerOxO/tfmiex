//rocDateUtil.js
function rocDateUtil() {
}
(function(global) {
	// setting
	var patternBeforeYear = '';
	var patternYear = 'YYYY';
	var patternAfterYear = 'MMDD';
	var elementclassName = 'datetimepickerROC1';
	// compute
	var patternPrefix = 'roc:';
	var pattern = patternBeforeYear + patternYear + patternAfterYear;
	var patternMark = patternPrefix + pattern;

	// handle moment.format
	global.moment.prototype.formatOrigin = global.moment.prototype.format;
	global.moment.prototype.format = function(inputPattern) {
		if (inputPattern != patternMark)
			return this.formatOrigin(inputPattern);
		var strBeforeYear = patternBeforeYear.length == 0 ? '' : this
				.formatOrigin(patternBeforeYear);
		var strAfterYear = patternAfterYear.length == 0 ? '' : this
				.formatOrigin(patternAfterYear);
		var rocDate = this.clone().subtract(1911, 'y');
		if (rocDate.year() < 0)
			rocDate.year(0);
		if (rocDate.year() > 999)
			rocDate.year(999);
		var strYear = rocDate.formatOrigin(patternYear).substr(1);
		return strBeforeYear + strYear + strAfterYear;
	};

	// handle datetimepicker.parseInputDate
	var parseInputDate = function(inputDate) {
		if (inputDate instanceof moment) {
			return moment(inputDate, pattern);
		}
		var patternYearLength = 3;
		if (patternBeforeYear.length + patternAfterYear.length + 2 == inputDate.length) {
			patternYearLength = 2;
		}
		var splitPoint1 = patternBeforeYear.length;
		var splitPoint2 = patternBeforeYear.length + patternYearLength;

		var subBeforeYear = inputDate.substr(0, splitPoint1);
		var subYear = inputDate.substr(splitPoint1, splitPoint2);
		var subAfterYear = inputDate.substr(splitPoint2);
		var rocStr = subBeforeYear + (parseInt(subYear) + 1911) + subAfterYear;
		return moment(rocStr, pattern);
	}

	// getMoment
	global.rocDateUtil.getMoment = function(inputDate, inputPattern) {
		if (!inputDate)
			return moment(inputDate);
		if (!inputPattern)
			inputPattern = global.dateFormatPattern1;
		var moment_ = parseInputDate(inputDate);
		return moment_;
	}

	// toCEFormat
	global.rocDateUtil.toCEFormat = function(inputDate, inputPattern) {
		if (!inputDate)
			return inputDate;
		var moment_ = global.rocDateUtil.getMoment(inputDate, inputPattern);
		return moment_.format(inputPattern);
	}

	// toROCFormat
	global.rocDateUtil.toROCFormat = function(inputDate) {
		if (!inputDate)
			return inputDate
		return moment(inputDate).format(patternMark);
	}

	global.rocDateUtil.init = function(item, option) {
		var setting = {
			format : patternMark,
			parseInputDate : parseInputDate
		};
		Object.assign(setting, option);
		var item = $(item);
		var children = item.children('input');
		var value = children.prop('value');
		item.datetimepicker(setting);
		if (value)
			children.prop('value', global.rocDateUtil.toROCFormat(value));
	}

	global.rocDateUtil.getPattern = function(item, option) {
		return patternBeforeYear + 'YYY' + patternAfterYear;
	}

	// init datetimepicker element
	$('.' + elementclassName).each(function(index, item) {
		global.rocDateUtil.init(item);
	});
}(this))
