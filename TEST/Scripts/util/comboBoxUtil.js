//註冊二級選單

function comboBoxAddSecondaryMenu(parentId, childId) {
    var parent = $('#' + parentId);
    var child = $('#' + childId);
    comboBoxAddSecondaryMenuByObj(parent, child);
}

function comboBoxAddSecondaryMenuByObj(parent, child) {
    var filterChilds = comboBoxGetFilterChildsFuncByObj(parent, child);
    parent.change(function(e) {
        filterChilds();
    });
    filterChilds(child.val());
}

function comboBoxGetFilterChildsFunc(parentId, childId) {
    var parent = $('#' + parentId);
    var child = $('#' + childId);
    return comboBoxGetFilterChildsFuncByObj(parent, child);
}

function comboBoxGetFilterChildsFuncByObj(parent, child) {
    var options = child.find('option').not(":eq(0)");
    return function(childValue) {
        var parentValue = parent.val();
        options.filter('[' + (parent.prop('name') || parent.prop('id')) + '="' + parentValue + '"]').appendTo(
            child);
        options.filter('[' + (parent.prop('name') || parent.prop('id')) + '!="' + parentValue + '"]').detach();
        childValue = (childValue == 0 || childValue) ? childValue : (child.val() == 0 || child.val()) ? child.val() : '';
        comboBoxVal(child, childValue);
    };
}

function comboBoxVal($elem, value, defValue) {
    $elem.val(value);
    if ($elem[0].selectedIndex > -1) {
        return;
    }
    if (defValue == 0 || defValue) {
    	comboBoxVal($elem, defValue);
        return;
    }
    $elem[0].selectedIndex = 0;
}
