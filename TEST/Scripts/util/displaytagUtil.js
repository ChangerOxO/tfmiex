var displaytagPattern = /d-\d+-[ops]/;

function displaytagAjaxCall(ajaxSettingParam, url, getDataFunc, tableId) {
	var ajaxSetting = {};
	Object.assign(ajaxSetting, ajaxSettingParam);

	displaytagInitAjaxDat(ajaxSetting, url, tableId);

	var successFunc = ajaxSettingParam.success;
	ajaxSetting.success = function(tbody) {
		if (successFunc)
			successFunc(tbody);

		displaytagInitPageItem(getDataFunc, tableId);
	}

	b2bAjaxCall(ajaxSetting);
}

function displaytagInitAjaxDat(ajaxSettingParam, url, tableId) {
	if (!url)
		return;

	var params = b2bParseUrl(url);
	ajaxSettingParam.data = params;

	var matchKey = Object.keys(params).find(function(key) {
		return displaytagPattern.test(key);
	})

	if (!matchKey)
		return;

	matchKey = matchKey.slice(0, -1);
	var resultPageNo = ajaxSettingParam.data[matchKey + "p"];
	var resultDesc = ajaxSettingParam.data[matchKey + "o"];
	var resultOrderByClause = ajaxSettingParam.data[matchKey + "s"];

	var orderByArray = $("#" + tableId + " th.page-item").map(
			function(index, item) {
				return $(item).attr("scope")
			});
	if (resultPageNo)
		ajaxSettingParam.data.pageNo = resultPageNo
	if (resultDesc)
		ajaxSettingParam.data.desc = (resultDesc > 1);
	if (resultOrderByClause)
		ajaxSettingParam.data.orderByClause = orderByArray[resultOrderByClause];
}

function displaytagInitPageItem(getDataFunc, tableId) {
	var this_ = window;

	if (!getDataFunc)
		return;
	this_.$("#" + tableId + " .page-item a").each(function(index, item) {
		var href = $(item).attr("href");
		if (!href) {
			return;
		}
		this_.$(item).attr("href", "javascript:void(0);");

		this_.$(item).click(function() {
			getDataFunc(href);
		});

		if (this_.$(item).hasClass('active')) {
			this_.displaytagRecordState(getDataFunc.name, href)
		}

	})
}

function displaytagRecordState(getDataFuncName, href) {
	var this_ = window;

	if (!this_.sessionStorage) {
		return;
	}
	var displaytagState = {
		getDataFuncName : getDataFuncName,
		href : href
	};
	this_.sessionStorage.setItem('displaytagState-'
			+ this_.location.pathname.replace(/^(.+)\/search$/g, '$1')
			+ this_.location.search, this_.JSON.stringify(displaytagState));
}

$(document).ready(
		function() {
			var this_ = window;
			if (!this_.sessionStorage) {
				return;
			}
			var displaytagState = this_.JSON.parse(this_.sessionStorage
					.getItem('displaytagState-'
							+ this_.location.pathname.replace(
									/^(.+)\/search$/g, '$1')
							+ this_.location.search));
			if (!(displaytagState instanceof Object)) {
				return;
			}
			if (!(this_[displaytagState.getDataFuncName] instanceof Function)) {
				return;
			}
			this_[displaytagState.getDataFuncName](displaytagState.href);
		})