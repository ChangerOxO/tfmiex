function updatePostCode(element) {
	var address = $(element).closest('.common-address');
	$(element).closest('.common-address').find('input[name=postCode]').prop('value', $(element).val());
}
function updateCityCode(element) {
	if($(element).val()){
		var address = $(element).closest('.common-address');
		var option = $(element).siblings('select').children('option[value=' + $(element).val() + ']');
		if (option.length > 0) {
			address.find('select[name=countries]').prop('value', option.text()).change();
			address.find('select[name=cityCodes]').prop('value', option.val()).change();
		} else {
			address.find('select[name=countries]').prop('value', '').change();
			address.find('select[name=cityCodes]').prop('value', '').change();
		}
	}
}